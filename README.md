Vulnerabilities in Cylance products can only be reported through BugCrowd and Hackerone.  
Under BugCrowd terms and conditions, Cylance explicitly does not allow disclosure of this issue.  

Timeline:  
2017-01-16	Reported vulnerability to Cylance via BugCrowd, as directed in https://www.cylance.com/security  
2017-01-17	Added more details to this vulnerability with the affected functions  
2017-01-17	Cylance acknowledged submission  
2017-01-23	Cylance accepted vulnerability as valid, and awarded $300  
2017-01-23	Queried if a CVE will be assigned, and if details may be publicly disclosed after fix  
2017-01-24	Cylance replied that a CVE will not be assigned, and they are not giving permission to disclose publicly  
2017-01-26	Queried for an estimated patch release date  
2017-01-26	Cylance replied the release date is estimated to be in March  
2017-03-14	Claimed and donated bounty to Metta Welfare Association  
